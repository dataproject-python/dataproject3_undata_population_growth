import csv
import matplotlib.pyplot as plt
import numpy as np

aseanCountriesFilePath = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT3/asean-countries.csv"
saarcCountriesFilePath = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT3/saarc-countries.csv"
populationEstimatedFilePath = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT3/population-estimates.csv"

# 1st CODE


def plotIndianPopulationOverYears(allYears, allYearsPopulation):

    # Creating Plot based on this data

    plt.figure(figsize=(15, 15))
    plt.bar(allYears, allYearsPopulation, ec='black')
    plt.xlabel('YEARS', fontsize=12)
    plt.ylabel('POPULATION', fontsize=12)
    plt.title('INDIA POPULATION OVER YEARS', fontsize=15)
    plt.xticks(allYears, rotation=90)
    plt.show()


def calculateIndianPopulationOverYears(populationEstimatedFilePath):

    file = open(populationEstimatedFilePath)
    csvreader = csv.DictReader(file)
    allYears = []
    allYearsPopulation = []
    for row in csvreader:
        countryName = row['Region']
        if countryName == 'India':
            year = int(row['Year'])
            population = row['Population']
            allYears.append(year)
            allYearsPopulation.append(population)
    file.close()
    return allYears, allYearsPopulation


def executeIndianPopulationOverYears():

    allYears, allYearsPopulation = calculateIndianPopulationOverYears(
        populationEstimatedFilePath)
    plotIndianPopulationOverYears(allYears, allYearsPopulation)


# 2nd CODE


def plotPopulationOfAseanCountriesIn2014(aseanCountryNames, aseanCountryPopulations):

    # Creating bar plot using all the data which we have

    plt.figure(figsize=(15, 10))
    plt.barh(aseanCountryNames, aseanCountryPopulations, ec='black')
    plt.xlabel('POPULATION', fontsize=12)
    plt.ylabel('COUNTRIES', fontsize=12)
    plt.title('POPULATION OF ASEAN COUNTRIES IN 2014', fontsize=15)
    plt.show()


def calculatePopulationOfAseanCountriesIn2014(aseanCountriesFilePath, populationEstimatedFilePath):

    file = open(aseanCountriesFilePath)
    csvreader = csv.DictReader(file)
    aseanCountries = {}
    for row in csvreader:        # Fetching all of the aseanCountries names from the file
        countryName = row['COUNTRIES']
        aseanCountries[countryName] = -1
    file.close()

    file = open(populationEstimatedFilePath)
    csvreader = csv.DictReader(file)

    allAseanCountriesPopulations = {}
    for row in csvreader:
        year = int(row['Year'])
        countryName = row['Region']
        population = row['Population']
        if year == 2014 and countryName in aseanCountries:
            allAseanCountriesPopulations[countryName] = population

    aseanCountryNames = list(allAseanCountriesPopulations.keys())
    aseanCountryPopulations = list(allAseanCountriesPopulations.values())

    file.close()

    return aseanCountryNames, aseanCountryPopulations


def executePopulationOfAseanCountriesIn2014():

    aseanCountryNames, aseanCountryPopulations = calculatePopulationOfAseanCountriesIn2014(
        aseanCountriesFilePath, populationEstimatedFilePath)
    plotPopulationOfAseanCountriesIn2014(
        aseanCountryNames, aseanCountryPopulations)


# 3rd CODE


def plotTotalPopulationOfSaarcCountriesForAllYears(allYears, yearsTotalPopulation):

    # Creating bar plot using all the data which we have

    plt.figure(figsize=(30, 8))
    plt.barh(allYears, yearsTotalPopulation, ec='black')
    plt.xlabel('POPULATION')
    plt.ylabel('YEAR')
    plt.title('TOTAL POPULATION OF SAARC COUNTRIES PER YEAR', fontsize=15)
    plt.xticks(yearsTotalPopulation, rotation=90)
    plt.show()


def calculateTotalPopulationOfSaarcCountriesForAllYears(saarcCountriesFilePath, populationEstimatedFilePath):

    file = open(saarcCountriesFilePath)
    csvreader = csv.DictReader(file)

    saarcCountries = {}        # Fetching all saarc countries from the file
    for row in csvreader:
        countryName = row['COUNTRIES']
        saarcCountries[countryName] = -1
    file.close()

    file = open(populationEstimatedFilePath)
    csvreader = csv.DictReader(file)
    yearWiseDataOfSaarcCountries = {}
    # Creating dictionary to map Population count for years in range 1950 to 2015
    for y in range(1950, 2016):
        yearWiseDataOfSaarcCountries[y] = 0

    for row in csvreader:
        year = int(row['Year'])
        countryName = row['Region']
        if countryName in saarcCountries:
            # Storing the populations counts in respective years
            yearWiseDataOfSaarcCountries[year] += float(row['Population'])

    allYears = list(yearWiseDataOfSaarcCountries.keys())
    yearsTotalPopulation = list(yearWiseDataOfSaarcCountries.values())

    file.close()

    return allYears, yearsTotalPopulation


def executeTotalPopulationOfSaarcCountriesForAllYears():

    allYears, yearsTotalPopulation = calculateTotalPopulationOfSaarcCountriesForAllYears(
        saarcCountriesFilePath, populationEstimatedFilePath)
    plotTotalPopulationOfSaarcCountriesForAllYears(
        allYears, yearsTotalPopulation)


# 4th CODE


def plotAseanPopulationVsYears(aseanCountries, updatedDataOfYears):

    # Creating columns for Grouped Bar Charts

    c1 = updatedDataOfYears[0]
    c2 = updatedDataOfYears[1]
    c3 = updatedDataOfYears[2]
    c4 = updatedDataOfYears[3]
    c5 = updatedDataOfYears[4]
    c6 = updatedDataOfYears[5]
    c7 = updatedDataOfYears[6]
    c8 = updatedDataOfYears[7]
    c9 = updatedDataOfYears[8]
    c10 = updatedDataOfYears[9]
    c11 = updatedDataOfYears[10]

    w = 0.15        # Width of column

    # Creating bars for Grouped Bar Charts

    bar1 = np.arange(0, len(c1)*2, 2)
    bar2 = [i+w for i in bar1]
    bar3 = [i+w for i in bar2]
    bar4 = [i+w for i in bar3]
    bar5 = [i+w for i in bar4]
    bar6 = [i+w for i in bar5]
    bar7 = [i+w for i in bar6]
    bar8 = [i+w for i in bar7]
    bar9 = [i+w for i in bar8]
    bar10 = [i+w for i in bar9]
    bar11 = [i+w for i in bar10]

    # Creating Grouped Chart using all columns and bars we declared

    plt.figure(figsize=(15, 25))
    plt.bar(bar1, c1, w, label="2004", ec='black')
    plt.bar(bar2, c2, w, label="2005", ec='black')
    plt.bar(bar3, c3, w, label="2006", ec='black')
    plt.bar(bar4, c4, w, label="2007", ec='black')
    plt.bar(bar5, c5, w, label="2008", ec='black')
    plt.bar(bar6, c6, w, label="2009", ec='black')
    plt.bar(bar7, c7, w, label="2010", ec='black')
    plt.bar(bar8, c8, w, label="2011", ec='black')
    plt.bar(bar9, c9, w, label="2012", ec='black')
    plt.bar(bar10, c10, w, label="2013", ec='black')
    plt.bar(bar11, c11, w, label="2014", ec='black')
    plt.xlabel("ASEAN COUNTRIES", fontsize=12)
    plt.ylabel("POPULATION", fontsize=12)
    plt.title("ASEAN POPULATION vs YEARS", fontsize=15)
    plt.xticks(bar1 + 5*w, aseanCountries, rotation=45)
    plt.legend()
    plt.show()


def calculateAseanPopulationVsYears(aseanCountriesFilePath, populationEstimatedFilePath):

    file = open(aseanCountriesFilePath)
    csvreader = csv.DictReader(file)
    aseanCountries = {}
    for row in csvreader:        # Fetching all of the aseanCountries names from the file
        countryName = row['COUNTRIES']
        aseanCountries[countryName] = -1
    file.close()

    # Creating dictionary to store population data yearwise for all 'asean' countries
    dataOfYears = {}
    for country in aseanCountries:
        dataOfYears[country] = [0 for i in range(11)]

    file = open(populationEstimatedFilePath)
    csvreader = csv.DictReader(file)

    for row in csvreader:
        year = int(row['Year'])
        countryName = row['Region']
        # Storing population data into for each year with respect to 'asean' countries
        if countryName in aseanCountries and year in range(2004, 2015):
            dataOfYears[countryName][year-2004] = float(row['Population'])

    updatedDataOfYears = []
    for j in range(11):
        temp = []
        for d in dataOfYears.values():
            temp.append(d[j])
        updatedDataOfYears.append(temp)

    file.close()
    return aseanCountries, updatedDataOfYears


def executeAseanPopulationVsYears():

    aseanCountries, updatedDataOfYears = calculateAseanPopulationVsYears(
        aseanCountriesFilePath, populationEstimatedFilePath)
    plotAseanPopulationVsYears(aseanCountries, updatedDataOfYears)


if __name__ == '__main__':

    executeIndianPopulationOverYears()
    executePopulationOfAseanCountriesIn2014()
    executeTotalPopulationOfSaarcCountriesForAllYears()
    executeAseanPopulationVsYears()
